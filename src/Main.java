public class Main {
    public static void main(String[] args) {
        Dispute egg = new Dispute("egg");
        Dispute chicken = new Dispute("chicken");
        egg.start();
        chicken.start();

        try {
            chicken.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (egg.isAlive()) {
            System.out.println("Яйцо было первое");
        } else {
            System.out.println("Курица была первой");
        }
    }
}
